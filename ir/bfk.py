#!/usr/bin/python
import re, sys, urllib2
## Nicholas' BFK.DE Search Tool.
if len(sys.argv) < 2:
    print "I need an IP address or Domain Name to search for.\n\tUsage: %s [IP|DOMAIN]" % sys.argv[0]
    sys.exit()
host = str(sys.argv[1])
bfksite = "http://www.bfk.de/bfk_dnslogger.html?query=" 
sitedata = urllib2.urlopen(bfksite + host)
print "[=] Nicholas' BFK Check Script" 
print "[+] Querying for %s" % host
for line in sitedata:
    if re.search(host, line, re.I):
        line = re.sub(host, host + "\n\t", line)
        line = re.sub("<[^>]+>","", line)
        line = re.sub("&nbsp;", "\t", line)
        line = re.sub("The server returned the following data:", "", line)
        print "[+] Result:\n\t" + line[:-1]
print "[=] All Finished!" 
