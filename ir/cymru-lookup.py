#!/usr/bin/python
import socket, fileinput, re
print "[::] Cymru IP lookup script. Provide me with the IPs:" 
data = fileinput.input()
cymru = "begin\n"
for line in data:
    try: 
        ipsearch = re.search("([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})", line)
        cymru += ipsearch.group(1) + "\n"
    except: pass
cymru += "end\n"
print "[+] Contacting Team Cymru's Whois Server..."
tcpsocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
tcpsocket.connect(("whois.cymru.com", 43))
tcpsocket.sendall(cymru)
partialresponse = tcpsocket.recv(1024)
response = partialresponse
while len(partialresponse) > 0:
    partialresponse = tcpsocket.recv(1024)
    response += partialresponse
print "[::] Response Data:\n%s" % response
tcpsocket.close()

