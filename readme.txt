This is a random collection of scripts I've found useful at various
stages of my life. I'd like to share for instances when you may come
across the similar problems. Please understand all of the scripts I
have posted here are free to use, abuse, modify, adjust and reshare.

Attribution would be nice, but isn't required. The tools or ideas
posted here are in commercially available products already. Sometimes
I came across instances where I couldn't afford a commercial product
or I was unable to wait for the procurement process to complete.

Othertimes I simply needed something for one day, and purchasing a
tool would have been wasteful. I'm sure my code is riddled with
bugs, it certainly isn't professional grade, but I'd be happy to 
update it with your improvements if you email me: nicholas-at-nma-dot-io

Have a great time!
